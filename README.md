# Git autopull

Is a set of scripts with the objective to authomatize the pull task on linux.

Used to share, files with other computers.

ONLY FOR PUBLIC REPOSITORIES

#### Use case

We tested it on share tinc public key files between hosts, the repository has all the tink keys of all the hosts of the network. So when a host push a new key, all the other authomatically add it to his hosts files.

The /etc/tinc/netname/hosts is a ln -s f the repository. So all the hosts are updated.

Used on https://gitlab.com/selankon/tinc-utils
