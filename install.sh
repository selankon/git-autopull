echo "* Installing script git-autopull.sh"
  gitautopull="git-autopull.sh"
  if [[ -f $gitautopull ]]; then
    autopulldest="/etc/init.d/$gitautopull"
    cp $gitautopull $autopulldest
    chmod +x $autopulldest
    update-rc.d $gitautopull defaults
    if [[ $? != 0 ]]; then
      echo "!ERROR: when update-rc $gitautopull defaults"
    fi
    echo "* Starting deatached autopull script"
    start-stop-daemon --start --background  --exec $autopulldest -- start
    if [[ $? != 0 ]]; then
      echo "!ERROR: starting start-stop-daemon"
    fi
  else
    echo "ERROR: can't find $gitautopull"
  fi
